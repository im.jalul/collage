#include <iostream>
#include <limits>

using namespace std;

void clrscr() {
  // cout << "\x1B[2J"; // Clear the screen
  // cout << "\x1B[0;0H"; // place cursor at home position
  system("clear");
}

void pause() {
  char __ignored[0];

  cout << "Press ENTER to continue...";
  cin.clear();
  cin.getline(__ignored, 0);
  cin.ignore( numeric_limits<streamsize>::max(), '\n' );
}

int * sortIndexByPoint(int *point, int *indexOfPoint, size_t size) {
  int limit = sizeof(point);
  int current = 0;
  int currentIndex = 0;

  for (int i = 0, j = 0; i < limit; i++) {
    current = point[i];
    currentIndex = indexOfPoint[i];
    j = i - 1;

    while (j >= 0 && point[j] < current) {
      point[j+1] = point[j];
      indexOfPoint[j+1] = indexOfPoint[j];
      j = j - 1;
    }

    point[j+1] = current;
    indexOfPoint[j+1] = currentIndex;
  }
}

void doLihatKlasemen(char *tim, int *win, int *los, int *drw) {
  int numOfTim = sizeof(tim);

  int points[8] = { 0 };
  for (int j = 0; j < numOfTim; j++) {
    points[j] = win[j] * 3 + drw[j];
  }

  int sortedIndex[] = { 0, 1, 2, 3, 4, 5, 6, 7 };
  size_t __sortedIndexSize = sizeof(points) / sizeof(points[0]);
  sortIndexByPoint(points, sortedIndex, __sortedIndexSize);

  clrscr();
  cout
      << "+=======================================+\n"
      << "| Klasemen Sementara                    |\n"
      << "+-----------+------+------+------+------+\n"
      << "| Tim       | M    | K    | S    | Poin |\n";

  for (int i = 0; i < numOfTim; i++) {
    int s = sortedIndex[i];
    int poi = win[s] * 3 + drw[s];
    cout<<"| "<<tim[s]<<"         | "<<win[s]<<"    | "<<los[s]<<"    | "<<drw[s]<<"    | "<<poi<<"    |"<<endl;
  }

  cout<< "+===========+======+======+======+======+\n";
  pause();
}

int findIndex(char* tim_array, char value) {
  int found = -1;

  for (int i = 0, limit = sizeof(tim_array); i < limit; i++) {
    if (tim_array[i] == value) {
      found = i;
      break;
    }
  }

  return found;
}

void doEntryPertandingan(char *tim, int *win, int *los, int *drw) {
  clrscr();

  char tim1, tim2;
  int skor1, skor2, tim_index1, tim_index2;

  cout
      << "+=======================================+\n"
      << "| Entry Pertandingan                    |\n"
      << "+---------------------------------------+\n";

  cout<< "| Tim Pertama :";   cin>>tim1;
  cout<< "| Skor        :";   cin>>skor1;
  cout<< "| Tim Kedua   :";   cin>>tim2;
  cout<< "| Skor        :";   cin>>skor2;

tim_index1 = findIndex(tim, tim1);
tim_index2 = findIndex(tim, tim2);

if (tim_index1 != -1 || tim_index2 != -1) {
  if (skor1 == skor2) {
    drw[tim_index1] += 1;
    drw[tim_index2] += 1;
  } else if (skor1 > skor2) {
    win[tim_index1] += 1;
    los[tim_index2] += 1;
  } else {
    los[tim_index1] += 1;
    win[tim_index2] += 1;
  }
}


  cout
      << "+---------------------------------------+\n"
      << "| Entry data BERHASIL ...               |\n"
      << "+---------------------------------------+\n";
  pause();
}

int main() {
  int option = 0;
  char tim[8] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H' };
  int win[8] = { 0 };
  int los[8] = { 0 };
  int drw[8] = { 0 };

  do {
    clrscr();

cout
    << "+=======================================+\n"
    << "| Program Klasemen Pertandingan Grup @8 |\n"
    << "+---------------------------------------+\n"
    << "| Menu :                                |\n"
    << "| [1] Lihat Klasemen                    |\n"
    << "| [2] Entry Pertandingan                |\n"
    << "| [0] Selesai                           |\n"
    << "+---------------------------------------+\n"
    << "\n"
    << "  Pilih Menu : ";
    
    cin>>option;

    switch (option) {
      case 1 : 
        doLihatKlasemen(tim, win, los, drw);
        break;

      case 2 :
        doEntryPertandingan(tim, win, los, drw);
        break;

      case 0 : break;
    }

  } while (option != 0);

  clrscr();
  cout << "Program terminated ..." << endl;
  return 0;
}